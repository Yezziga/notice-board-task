/**
 * This file and its containing functions are used by 'diary.html' found in /templates.
 *
 * It uses the functions in 'api.js' to fetch and post data to the server, and
 * also contain functions for rendering the html based on user actions.
 *
 */

const TITLE = document.getElementById('title')
const INPUT_TITLE = document.getElementById('inputTitle')
const CREATOR = document.getElementById("noticeCreator")
const TEXT_AREA = document.getElementById('textArea')
const DATE = document.getElementById('date')
const TIME = document.getElementById('time')
const IMAGE = document.getElementById('image')
const url = window.location.pathname


async function loadDiaryData() {
    const id = url.substring(url.lastIndexOf('=') + 1)

    const response = await getNoticeById(id)

    const json = await response.json()

    if(!json.data) {
        console.log("no data")
        showAlert(json.message)
        return
    }

    addDataToHtml(json.data)

}


function addDataToHtml(data) {
    // Date and time is received as one string. Separate them to place
    // in its corresponding html field.
    let dateAndTime = data.date.split(/(\s+)/)
    DATE.value = dateAndTime[0]
    TIME.value = dateAndTime[2].substring(0, dateAndTime[2].lastIndexOf(':'))
    CREATOR.innerText = data.creator
    TITLE.innerText = 'Title: ' + data.title
    INPUT_TITLE.value = data.title
    TEXT_AREA.value = data.text
    if(!notLoggedIn() && getCookie("userName")===data.creator) {
        console.log("adding btns")
        addBtns()
    } else {
        addReplyBtn()
    }
    if(data.replies !==null) {
        let replies = data.replies
            let tbody = document.getElementById('reply-table-body')

            let row = 1
            replies.forEach(diary => {
                let tr = document.createElement('tr')

                let thRow = document.createElement('th')
                thRow.setAttribute('scope', 'row')
                let tdDate = document.createElement('td')

                // Add title as link element
                let tdText = document.createElement('td')
                let text = document.createElement('p')
                text.innerText = diary.text
                tdText.appendChild(text)

                thRow.innerText = '' + row++
                tdDate.innerText =  diary.date
                // tdDate.innerText =  diary.author + ", " + diary.date

                tr.appendChild(thRow)
                tr.appendChild(tdText)
                tr.appendChild(tdDate)

                tbody.appendChild(tr)
            })

    }
}

/* Enable and toggle hide html elements */
function enableEditDiary() {
    setInputFieldsDisabledValue(false)

    INPUT_TITLE.hidden = false
    TITLE.hidden = true
    IMAGE.hidden = true

    document.getElementById('editButton').hidden = true
    document.getElementById('deleteButton').hidden = true;
    document.getElementById('replyButton').hidden = true;

    document.getElementById('inputTitle-label').hidden = false
    document.getElementById('confirmButton').hidden = false
}

/* Disable and toggle hide html elements */
function disableEditDiary() {
    setInputFieldsDisabledValue(true)

    INPUT_TITLE.hidden = true
    TITLE.hidden = false
    IMAGE.hidden = false

    document.getElementById('editButton').hidden = false
    document.getElementById('deleteButton').hidden = false;
    document.getElementById('replyButton').hidden = false;

    document.getElementById('inputTitle-label').hidden = true
    document.getElementById('confirmButton').hidden = true
}

async function confirmUpdate() {
    const id = url.substring(url.lastIndexOf('=') + 1)

    let data = {title: INPUT_TITLE.value, text: TEXT_AREA.value, date: DATE.value + ' ' + TIME.value + ':00'}
    const response = await updateNotice(data, id)

    const json = await response.json()

    addDataToHtml(json.data)

    disableEditDiary()
}

async function removeDiary() {
    const id = url.substring(url.lastIndexOf('/') + 1)
    const response = await deleteNotice(id)

    const json = await response.json()

    document.getElementById('success-text').innerText = json.message
    document.getElementById('success-alert').hidden = false
    document.getElementById('diary-info-container').hidden = true
}

/**
 * Set the input fields of the HTML to input boolean value.
 * @param {boolean} boolean
 */
function setInputFieldsDisabledValue(value) {
    INPUT_TITLE.disabled = value
    TEXT_AREA.disabled = value
    DATE.disabled = value
    TIME.disabled = value
}

function showAlert(message) {
    let alertText = document.getElementById('error-text')
    alertText.innerText = message
    document.getElementById('error-alert').hidden = false

    document.getElementById('diary-info-container').hidden = true
}
