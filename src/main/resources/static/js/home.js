/**
 * This file and its containing functions are used by 'index.html' found in /static.
 *
 */

async function loadNotices() {
    const response = await getNotices()

    const json = await response.json()

    addNoticesToHtmlTable(json.data)
}

/**
 * Iterates all diaries and adds them to a HTML table.
 * @param {object[]} diaries
 */
function addNoticesToHtmlTable(diaries) {
    let tbody = document.getElementById('diary-table-body')

    let row = 1
    diaries.forEach(diary => {
        let tr = document.createElement('tr')

        let thRow = document.createElement('th')
        thRow.setAttribute('scope', 'row')
        let tdDate = document.createElement('td')

        // Add title as link element
        let tdTitle = document.createElement('td')
        let a = document.createElement('a')
        a.setAttribute('href', '/noticeId=' + diary.id)    // Set href to this diary's endpoint
        a.innerText = diary.title
        tdTitle.appendChild(a)

        thRow.innerText = '' + row++
        tdDate.innerText = diary.date

        tr.appendChild(thRow)
        tr.appendChild(tdTitle)
        tr.appendChild(tdDate)

        tbody.appendChild(tr)
    })
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
}

function notLoggedIn() {
    var myCookie = getCookie("userName");
    // console.log(myCookie==null)
    if(myCookie==null) {
        // no cookie
        return true
    } else {
        return false
    }

}

function addLoginOption() {
    let wrapper = document.createElement("a")
    wrapper.href="/login"
    wrapper.innerText = "Log in"
    wrapper.className = "nav-link"
    let li = document.createElement("li")
    wrapper.appendChild(li)
    document.getElementById("navbar-list").appendChild(wrapper)

}

function addAddOption() {
    let wrapper = document.createElement("a");
    wrapper.href="/notice/add";
    wrapper.innerText = "Add Notice";
    wrapper.className = "nav-link"
    let li = document.createElement("li");
    wrapper.appendChild(li);
    document.getElementById("navbar-list").appendChild(wrapper);
}

