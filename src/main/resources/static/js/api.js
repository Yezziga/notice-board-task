
// POST REQUEST
/**
 * Create a diary with the input data
 * @param {Object} data
 * @returns {Promise<Response>}
 */
async function addNotice(data) {
    const response = await fetch("/notice/add", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

// GET REQUESTS
/**
 * Fetches all diaries from database.
 * @returns {Promise<Response>}
 */
async function getNotices() {
    const response = await fetch("/notices")

    return response
}

/**
 * Fetch a specific diary from the database based on its id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function getNoticeById(id) {
    const newId = url.substring(url.lastIndexOf('=') + 1)
    // console.log(newId)
    const response = await fetch("/view/noticeId=" + newId)

    return response
}

// PATCH REQUEST
/**
 * Update the diary with the input data.
 * @param {object} data
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function updateNotice(data, id) {
    const response = await fetch("/edit/noticeId=" + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

// DELETE REQUEST
/**
 * Sends a delete request to the API for the input id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function deleteNotice(id) {
    const newId = url.substring(url.lastIndexOf('=') + 1)

    console.log("@delete id:: " + newId)
    const response = await fetch("/edit/noticeId=" + newId, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })

    return response
}

// ########## REPLY
//##################################
async function addReply(data) {
    const url = window.location.pathname;
    const id = url.substring(url.lastIndexOf('=') + 1)
    console.log("dumb id " + id)
    const response = await fetch("/noticeId=" + id, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

// GET REQUESTS
/**
 * Fetches all diaries from database.
 * @returns {Promise<Response>}
 */
async function getReplies() {
    const response = await fetch("/replies")

    return response
}

/**
 * Fetch a specific diary from the database based on its id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function getReplyById(id) {
    const newId = url.substring(url.lastIndexOf('=') + 1)
    // console.log(newId)
    const response = await fetch("/view/replyId=" + newId)

    return response
}