/**
 * This file and its containing functions are used by 'add-entry.html' found in /templates.
 *
 */

const SUCCESS_ALERT = document.getElementById('success-alert')

async function submitAddReply() {
    resetAlerts()

    // READ VALUES
    let text = document.getElementById('inputText').value
    let date = document.getElementById('inputDate').value
    let time = document.getElementById('inputTime').value
    let username = await getCookie("userName")

    var reg = /^\d+$/;
    if ( !text || !date || !time.substring(0,2).match(reg)
        || !time.substring(3,5).match(reg)) {
        showAlert(null, 'Invalid or empty input! Try again.')
        return
    }
    time += ':00' // Always set seconds to 00 (because of database Timestamp format)
    date += ' ' + time // Time is a part of the date in the database, so append time to date
    let data = {creator: username, text: text, date: date}
    console.log(username + " " + text  + " " + date + " " + time)
    let response = await addReply(data)

    const status = response.status
    let json = await response.json()
    const message = json.message

    await showAlert(status, message)
}

function showAlert(status, message) {
    let alertText
    if (status === 201) {
        alertText = document.getElementById('success-text')
        alertText.innerText = message
        document.getElementById('success-alert').hidden = false
    }
    else {
        alertText = document.getElementById('error-text')
        alertText.innerText = message
        document.getElementById('error-alert').hidden = false
    }
}

// Alerts are set to their default state 'hidden'
function resetAlerts() {
    document.getElementById('error-alert').hidden = true
    document.getElementById('success-alert').hidden = true
}


function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
}