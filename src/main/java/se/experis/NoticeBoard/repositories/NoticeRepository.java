package se.experis.NoticeBoard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.models.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(String id);
}
