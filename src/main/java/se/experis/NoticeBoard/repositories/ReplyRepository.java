package se.experis.NoticeBoard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.models.Reply;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Reply getById(String id);
}
