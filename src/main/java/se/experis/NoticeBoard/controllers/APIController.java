package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.models.*;
import se.experis.NoticeBoard.repositories.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@RestController
public class APIController {
    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private ReplyRepository replyRepository;

    @PostMapping("/notice/add")
    public ResponseEntity<CommonResponse> addNotice(HttpServletRequest request, @RequestBody Notice notice) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        System.out.println(notice.creator + " , ");
        if(isValidDiary(notice)) {
            notice = noticeRepository.save(notice);

            System.out.println("New notice with id: " + notice.id);
            response = HttpStatus.CREATED;
            cr.data  = notice;
            cr.message = "Notice created";

        } else {
            cr.message = "Not valid notice inputs";
            response = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(cr, response);
    }

    @PatchMapping("/edit/noticeId={id}")
    public ResponseEntity<CommonResponse> updateDiary(HttpServletRequest request, @RequestBody Notice newDiary, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (noticeRepository.existsById(id)) {
            Notice diary = noticeRepository.findById(id).get();
            if (isValidDiary(newDiary)) {
                diary.title = newDiary.title;
                diary.text = newDiary.text;
                diary.date = newDiary.date;
            }
            noticeRepository.save(diary);

            cr.data = diary;
            cr.message = "Updated diary with id: " + diary.id;
            System.out.println("Updated diary with id: " + diary.id);
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find diary wih id: " + id;
            System.out.println("Did not find diary wih id: " + id);
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    @DeleteMapping("/edit/noticeId={id}")
    public ResponseEntity<CommonResponse> deleteDiary(HttpServletRequest request, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (noticeRepository.existsById(id)) {
            noticeRepository.deleteById(id);
            cr.message = "Deleted diary with id: " + id;
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find diary with id: " + id;
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    @PostMapping("/noticeId={id}/add-reply")
    public ResponseEntity<CommonResponse> addReply(HttpServletRequest request, @RequestBody Reply reply, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response = HttpStatus.NOT_FOUND;

        if(noticeRepository.existsById(id)) {
            Notice notice = noticeRepository.findById(id).get();
            reply.setNotice(notice);

            reply = replyRepository.save(reply);
            System.out.println("New reply with id: " + reply.id + " for notice with id: " + id);
            cr.data = reply;
            cr.message = "Reply created";
            response = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "Could not create reply: notice with id " + id + " does not exist";
        }

        return new ResponseEntity<>(cr, response);
    }


    @GetMapping("/view/noticeId={id}")
    public ResponseEntity<CommonResponse> getNoticeById(HttpServletRequest request, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        System.out.println("getting notice");
        if (noticeRepository.existsById(id)) {
            cr.data = noticeRepository.findById(id);
            cr.message = "Diary with id " + id;
            response = HttpStatus.OK;
            System.out.println("Notice fetched");
        }
        else {
            cr.data = null;
            cr.message = "Diary not found";
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    @GetMapping("/notices")
    public ResponseEntity<CommonResponse> getNotices(HttpServletRequest request) {

        List<Notice> notices = noticeRepository.findAll();
        Collections.sort(notices, Collections.reverseOrder());
        CommonResponse cr = new CommonResponse();
        cr.data = notices;
        cr.message = "Returning all notices";

        System.out.println("Returning all notices");

        HttpStatus response = HttpStatus.OK;

        return new ResponseEntity<>(cr, response);
    }
    /**
     * Checks if the diary contains information. A diary cannot be created if
     * it does not contain title, text or date.
     * @param notice the diary to check
     * @return true if valid, else false
     */
    private boolean isValidDiary(Notice notice) {
        return notice.date!=null && !notice.text.isEmpty() && !notice.title.isEmpty();
    }

}
