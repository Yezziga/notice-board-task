package se.experis.NoticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.models.Notice;
import se.experis.NoticeBoard.models.Reply;
import se.experis.NoticeBoard.repositories.NoticeRepository;
import se.experis.NoticeBoard.repositories.ReplyRepository;
import se.experis.NoticeBoard.utils.SessionKeeper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private ReplyRepository replyRepository;
    private HashMap<String, String> users;

    @GetMapping("/login")
    public String showLogInPage() {

        return "login-page";
    }

    @PostMapping(value="/saveSession")
    public String add(@RequestParam("username-input") String userName, @RequestParam("password-input") String password,
                      HttpServletResponse response, HttpSession session) {
        if(isValidUser(userName, password)) {
            Cookie cookie = new Cookie("userName", userName);
            cookie.setMaxAge(600);
            response.addCookie(cookie);
            System.out.println("session created, logging in");
        }
        System.out.println("Login failed");
        return "redirect:/index.html";
    }

    @RequestMapping(value = "/notice/add", method = RequestMethod.GET)
    public String showAddNoticePage(Model model) {
        model.addAttribute("notice", new Notice());
        return "add-notice-page";
    }

    @GetMapping("/noticeId={id}")
    public String getNotice(@PathVariable("id") Integer id) {
        return "view-notice-page";
    }

    @GetMapping("/noticeId={id}/add-reply")
    public String showAddReplyPage(Model model, @PathVariable("id")Integer id) {
        model.addAttribute("reply", new Reply());
        return "add-reply-page";
    }

    private boolean isValidUser(String userName, String password) {
        if(users== null) {
            users = new HashMap<>();
            users.put("Craig", "cat");
            users.put("Jessica", "123");
        }
        Iterator it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            String validUsername = (String) pair.getKey();
            String validPassword = (String) pair.getValue();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            System.out.println(userName.equals(validUsername) + "  " + password.equals(validPassword));
            if(userName.equals(validUsername) && password.equals(validPassword)) {
                return true;
            }
            it.remove();
        }
        return false;
    }



}
