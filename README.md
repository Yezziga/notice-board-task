Username: password
Craig   cat OR
Jessica 123

Heroku: https://notice-board-jq.herokuapp.com

Implemented functions:
- Sorted list of notices on default page
- Log in (username stored as a cookie)
- View notices
- Add/edit/delete own notices (logged in only)
- Add reply to a notice
- View replies of a notice

Not implemented/finished functions:
- Log out after 10 min of inactivity (currently "logs out" after 10 min)